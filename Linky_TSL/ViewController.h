//
//  ViewController.h
//  Linky_TSL
//
//  Created by SakethReddy on 26/02/16.
//  Copyright (c) 2016 Trusted Software Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
@interface ViewController : UIViewController<SlideNavigationControllerDelegate>


@end

