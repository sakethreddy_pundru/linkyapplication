//
//  ViewController.m
//  Linky_TSL
//
//  Created by SakethReddy on 26/02/16.
//  Copyright (c) 2016 Trusted Software Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:128/255.0f green:27/255.0f blue:128/255.0f alpha:1.0f];
}

-(BOOL)slideNavigationControllerShouldDisplayRightMenu{
    return YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
