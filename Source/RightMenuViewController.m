//
//  RightMenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/26/14.
//  Copyright (c) 2014 Aryan Ghassemi. All rights reserved.
//

#import "RightMenuViewController.h"

@implementation RightMenuViewController{
    
    UITableView     *table;
    NSString        *value1;
    NSArray         *array;
    NSMutableArray  *mainArray;
    NSURL           *url;
    UIButton        *button;
    UILabel         *label;
    
}

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor       = [UIColor lightGrayColor];
    
    UIImageView *imageView              = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightMenu.jpg"]];
    self.tableView.backgroundView       = imageView;
    
    UILabel   *name                     = [[UILabel alloc]init];
    name.frame                          = CGRectMake (self.view.frame.origin.x+90, self.view.frame.origin.y+50, self.view.frame.size.width-180, 20);
    name.text                           = @"Hello";
    [self.view addSubview:name];
    
    UILabel   *email                    = [[UILabel alloc]init];
    email.frame                         = CGRectMake(self.view.frame.origin.x+90, self.view.frame.origin.y+75, self.view.frame.size.width-180, 20);
    email.text                          = @"World";
    [self.view addSubview:email];
    
    mainArray                           =  [[NSMutableArray alloc]init];
    
    array                               = @[
                                            @{
                                                @"title":@"HOME",
                                                @"image":@"ic_home"
                                                }
                                            ,@{
                                                @"title":@"FAVORITE",
                                                @"image":@"ic_favorite_border"
                                                }
                                            ,@{
                                                @"title":@"SECURE",
                                                @"image":@"ic_visibility"
                                                }
                                            ,@{
                                                @"title":@"DRAFT",
                                                @"image":@"ic_list"
                                                }
                                            ,@{
                                                @"title":@"SHARED",
                                                @"image":@"ic_share"
                                                }
                                            ,@{
                                                @"title":@"TAGS",
                                                @"image":@"ic_loyalty"
                                                }
                                            ,@{
                                                @"title":@"PROFILE",
                                                @"image":@"ic_person_outline"
                                                }
                                            ,@{
                                                @"title":@"SETTINGS",
                                                @"image":@"ic_build"
                                                }
                                            ,@{
                                                @"title":@"LOGOUT",
                                                @"image":@"ic_power_settings_new"
                                                }
                                            ];
    
    
    
    [mainArray addObject:array];
    
    table                               = [[UITableView alloc]init];
    table.frame                         = CGRectMake(0, self.view.frame.origin.y+100, self.view.frame.size.width, self.view.frame.size.height-100);
    table.delegate                      = self;
    table.dataSource                    = self;
    
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    table.separatorColor                = [UIColor whiteColor];
    table.tableFooterView               = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:table];
    
    
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  mainArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[mainArray objectAtIndex:section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view                        = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
    view.backgroundColor                = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *STRING             = @"cell";
    
    UIImageView *imageview;
    
    UITableViewCell *cell           = [tableView dequeueReusableCellWithIdentifier:STRING];
    if (cell==nil) {
        cell                    = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STRING];
        
        imageview               = [[UIImageView alloc]initWithFrame:CGRectMake(90, 20, 20, 20)];
        [cell addSubview:imageview];
        
        label                   = [[UILabel alloc]init];
        label.frame             = CGRectMake(140, 20, 150, 20);
        
        [cell addSubview:label];
        
    }
    
    NSArray *currentArray           = [mainArray objectAtIndex:indexPath.section];
    NSDictionary *mdict             = [currentArray objectAtIndex:indexPath.row];
    label.text                      = [mdict valueForKey:@"title"];
    label.font                      = [UIFont fontWithName:@"Helvetica" size:13];
    imageview.image                 = [UIImage imageNamed:[mdict valueForKey:@"image"]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard        = [UIStoryboard storyboardWithName:@"Main"
                                                                    bundle: nil];
    
    UIViewController *vc ;
    
    switch (indexPath.row)
    {
        case 0:
            label.highlighted = YES;
            [label setHighlightedTextColor:[UIColor colorWithRed:128/255.0f green:27/255.0f blue:128/255.0f alpha:1.0f]
             ];
            vc                          = [mainStoryboard instantiateViewControllerWithIdentifier: @"ViewController"];
            break;
            
        case 1:
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            return;
            break;
            
            
    }
    
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
    
    
}

@end
